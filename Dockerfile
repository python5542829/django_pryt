FROM python:latest

RUN useradd -m yo
WORKDIR /app

USER yo
COPY --chown=yo:yo requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

USER root
COPY . .
RUN chown -R yo:yo /app

USER yo

EXPOSE 8081

CMD ["python", "manage.py", "runserver", "0.0.0.0:8081"]