from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Room
from django import forms


class RoomForm(ModelForm):
    class Meta:
        model = Room
        fields = ['topic', 'name', 'description']


class LoginForm(forms.Form):
    username = forms.CharField(label="Username",
                               widget=forms.TextInput(
                                   attrs={
                                       'placeholder': 'Enter your username...',
                                       # 'class': 'form-control border-secondary rounded-pill'
                                   }
                               )
                               )
    password = forms.CharField(label="Password",
                               widget=forms.TextInput(
                                   attrs={
                                       'placeholder': 'Enter your password...',
                                       'type': 'password',
                                       #'class': 'form-control border-secondary rounded-pill'
                                   }
                               )
                               )


class UserRegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Required.')
    last_name = forms.CharField(max_length=30, required=True, help_text='Required.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'password1', 'password2', )