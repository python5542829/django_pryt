from django.contrib.auth.decorators import login_required
from django.urls import path
from . import views
from .views import CreateRoom, UpdateRoom, DeleteRoom, LoginView, SignUpView, RoomView, DeleteMessage

app_name = "root"
urlpatterns = [
    path('', views.main_page, name="main-page"),

    # Rooms
    path('room/<int:pk>/', RoomView.as_view(), name="room"),
    path('create-room/', login_required(CreateRoom.as_view(), login_url='root:login'), name="create-room"),
    path('update-room/<int:pk>', login_required(UpdateRoom.as_view(), login_url='root:login'), name="update-room"),
    path('delete-room/<int:pk>', login_required(DeleteRoom.as_view(), login_url='root:login'), name="delete-room"),

    # User authentication
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', views.logout_user, name='logout'),
    path('sign-up/', SignUpView.as_view(), name='sign-up'),

    # Messages
    path('delete-message/<int:pk>', login_required(DeleteMessage.as_view()), name='delete-message'),

    # User profile
    path('profile/<int:pk>', views.user_profile, name="user-profile")
]
