from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import render, redirect

from .models import Room, Topic, Message
from django.views import View
from .forms import RoomForm, LoginForm, UserRegisterForm


def main_page(request):
    query = request.GET.get('q') if request.GET.get('q') is not None else ''
    rooms = Room.objects.filter(
        Q(topic__name__icontains=query) |
        Q(name__icontains=query) |
        Q(description=query)
    )
    topics = Topic.objects.all()
    context = {'rooms': rooms, 'topics': topics}

    return render(request, 'root/main_page.html', context)


class RoomView(View):
    def dispatch(self, request, *args, **kwargs):
        self.room = Room.objects.get(id=kwargs['pk'])
        self.messages = self.room.message_set.all().order_by('created')
        self.participants = self.room.participants.all()
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk):
        context = {
            'room': self.room,
            'messages': self.messages,
            'participants': self.participants
        }
        return render(request, 'root/room.html', context)

    def post(self, request, pk):
        Message.objects.create(
            user=request.user,
            room=self.room,
            body=request.POST.get('body')
        )
        self.room.participants.add(request.user)
        return redirect('root:room', pk=self.room.id)


class CreateRoom(View):
    def dispatch(self, request, *args, **kwargs):
        self.form = RoomForm(request.POST or None)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        context = {'form': self.form}
        return render(request, 'root/room_form.html', context)

    def post(self, request):
        if self.form.is_valid():
            room = self.form.save(commit=False)
            room.host = request.user
            room.save()
            return redirect('root:main-page')


class UpdateRoom(View):
    def dispatch(self, request, *args, **kwargs):
        self.room = Room.objects.get(id=kwargs['pk'])
        if request.user != self.room.host:
            return render(request, 'root/user_error.html', {'message': 'You are not allowed to access this page'})
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk):
        form = RoomForm(instance=self.room)
        context = {'form': form}
        return render(request, 'root/room_form.html', context)

    def post(self, request, pk):
        form = RoomForm(request.POST, instance=self.room)
        if form.is_valid():
            form.save()
            return redirect('root:main-page')


class DeleteRoom(View):
    def dispatch(self, request, *args, **kwargs):
        self.room = Room.objects.get(id=kwargs['pk'])
        if request.user != self.room.host:
            return render(request, 'root/user_error.html', {'message': 'You are not allowed to access this page'})
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk):
        context = {'element': self.room}
        return render(request, 'root/delete.html', context)

    def post(self, request, pk):
        self.room.delete()
        return redirect('root:main-page')


class LoginView(View):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('root:main-page')

        self.form = LoginForm(request.POST or None)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        context = {'form': self.form}
        return render(request, 'root/login.html', context)

    def post(self, request):
        if self.form.is_valid():
            username = self.form.cleaned_data['username'].lower()
            password = self.form.cleaned_data['password']

            try:
                User.objects.get(username=username)
            except User.DoesNotExist:
                return render(self.request, 'root/user_error.html', {'message': 'User not found'})

            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('root:main-page')
            else:
                return render(self.request, 'root/user_error.html', {'message': 'Invalid credentials'})


def logout_user(request):
    logout(request)
    return redirect('root:main-page')


class SignUpView(View):
    def dispatch(self, request, *args, **kwargs):
        self.form = UserRegisterForm(request.POST or None)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        context = {'form': self.form}
        return render(request, 'root/sign_up.html', context)

    def post(self, request):
        if self.form.is_valid():
            user = self.form.save(commit=False)
            user.username = user.username.lower()
            user.save()
            return redirect('root:login')
        else:
            return render(self.request, 'root/user_error.html', {'message': 'Invalid credentials: passwords do not '
                                                                            'match'})


class DeleteMessage(View):
    def dispatch(self, request, *args, **kwargs):
        self.message = Message.objects.get(id=kwargs['pk'])
        self.room = Room.objects.get(id=request.GET.get('pk2'))
        if request.user != self.message.user:
            return render(request, 'root/user_error.html', {'message': 'You are not allowed to access this page'})
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk):
        context = {'element': self.message}
        return render(request, 'root/delete.html', context)

    def post(self, request, pk):
        self.message.delete()

        user_messages = Message.objects.filter(room=self.room, user=request.user)

        if not user_messages.exists():
            # If the user has no messages, remove them from the room participants
            self.room.participants.remove(request.user)

        return redirect('root:room', pk=self.room.id)


def user_profile(request, pk):
    user = User.objects.get(id=pk)
    rooms = user.room_set.all()
    messages = user.message_set.all()
    topics = Topic.objects.all()
    room_count = rooms.count()
    context = {
        'user': user,
        'rooms': rooms,
        'messages': messages,
        'topics': topics,
        'room_cout': room_count
    }
    return render(request, 'root/profile.html', context)
